-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------


local LEFT, UP, RIGHT, DOWN = 0, 1, 2, 3
local EARTH = 0
local GRASS = 1
local SKY = 2
local HOLE = 3
local COIN = 4
local PLAYER = 5
local ENEMY = 6
local SPRITE_W = 1
local SPRITE_H = 1
local X_MAX = 20
local Y_MAX = 15


-- create the display grid
-- since these blocks are permanently added to the display,
-- all the game needs to do is change the colours of the blocks
-- by calling:
--
--   blocks[ypos][xpos]:setFillColor(...)
--
local blocks = {}
for row = 0, Y_MAX-1, 1 do
    local tmp = {}
    for col = 0, X_MAX-1, 1 do
        local newRectangle = display.newRect( 0, 0, 16, 16 )
        newRectangle.strokeWidth = 1
        newRectangle:setFillColor( col/X_MAX, row/Y_MAX, 1 )
        newRectangle:setStrokeColor( 0, 0, 0 )
        newRectangle.x = display.contentCenterX + (col-X_MAX/2)*16
        newRectangle.y = display.contentCenterY + (row-Y_MAX/2) * 16
        tmp[col] = newRectangle
    end
    blocks[row] = tmp
end


local tiles = { }
for row = 0, Y_MAX-1, 1 do
    local tmp = {}
    for col = 0, X_MAX-1, 1 do
        tmp[col] = 0
    end
    tiles[row] = tmp
end


local speed = 20
local dig_pos_x = 10
local dig_pos_y = 6

local enemy_pos_x = 0
local enemy_pos_y = 5
local enemy_state = 0
local enemy_dir = RIGHT

local dig_dir = DOWN


local function draw_tile( t, r, c )
    local function unpackRGB(rgb)
        return rgb[1]/255, rgb[2]/255, rgb[3]/255
    end
    local colours = {
        [EARTH]  = {170, 85, 0},
        [GRASS]  = {65, 255, 0},
        [SKY]    = {0, 125, 255},
        [HOLE]   = {0, 0, 0},
        [COIN]   = {255, 255, 0},
        [PLAYER] = {255, 255, 255},
        [ENEMY]  = {255, 0, 0},
    }
    blocks[r][c]:setFillColor( unpackRGB( colours[t] ) )
end

local function draw_tiles()
    local coins = 0
    for Y = 0, Y_MAX-1, 1 do
        for X = 0, X_MAX-1, 1 do
            draw_tile(tiles[Y][X], Y*SPRITE_W, X*SPRITE_H)
            if tiles[Y][X] == COIN then
                coins = coins + 1
            end
            if X == dig_pos_x and Y == dig_pos_y then
                draw_tile( PLAYER, Y*SPRITE_W, X*SPRITE_H)
            end
            if X==enemy_pos_x and Y==enemy_pos_y then
                draw_tile( ENEMY, Y*SPRITE_W, X*SPRITE_H)
            end
        end
    end
  
    return coins
end

local function clockwiseTurn(dir)
    return (dir + 1) % 4
end


local function move(x, y, dir)
    local xoffset = {
        [LEFT] = -1, [UP] = 0, [RIGHT] = 1, [DOWN] = 0,
    }
    local yoffset = {
        [LEFT] =  0, [UP] =-1, [RIGHT] = 0, [DOWN] = 1,
    }
    x = x + xoffset[dir]
    y = y + yoffset[dir]
    if x >= 0 and x < X_MAX and y < Y_MAX and y >= 6 then
        return x,y
    else
        return
    end
end


local function updatePlayer()
    local newx,newy = move(dig_pos_x, dig_pos_y, dig_dir)
    if newx then
        dig_pos_x = newx
        dig_pos_y = newy
        tiles[dig_pos_y][dig_pos_x] = HOLE
    end
end

local function updateEnemy(count)
    if count<0 then
        return
    end

    if enemy_state == 0 then
        enemy_pos_x = enemy_pos_x + 1
        if enemy_pos_x == 10 then
            enemy_state = 1
            enemy_dir = DOWN
        end
    elseif enemy_state == 1 then
        local newx, newy = move( enemy_pos_x, enemy_pos_y, enemy_dir)

        if newx and tiles[newy][newx] == HOLE then
            enemy_pos_x = newx
            enemy_pos_y = newy
        else
            enemy_dir = clockwiseTurn( enemy_dir )
            updateEnemy(count-1)
        end
    end
end

local function setup()
    dig_pos_x = 10
    dig_pos_y = 6
    dig_dir = DOWN

    enemy_pos_x = 0
    enemy_pos_y = 5
    enemy_state = 0
    enemy_dir = RIGHT

    fillPatterns = {
        { 0, 5-1, SKY },
        { 5, 6-1, GRASS },
        { 6, Y_MAX-1, EARTH },
    }

    for key, pattern in ipairs(fillPatterns) do
        for Y = pattern[1], pattern[2], 1 do
            for X = 0, X_MAX-1, 1 do
                tiles[Y][X] = pattern[3]
            end
        end
    end

    tiles[dig_pos_y][dig_pos_x] = HOLE
    tiles[dig_pos_y-1][dig_pos_x] = SKY

    for i = 1, 6, 1 do
        tiles[math.random(Y_MAX-6)+6-1][math.random(X_MAX)-1] = COIN
    end  
end

local accumMillis = 0
local function update(event)
    coins = draw_tiles()

    if coins == 0 or ( dig_pos_x == enemy_pos_x and
                        dig_pos_y == enemy_pos_y ) then
        --//speed *= 0.75;
        setup()
    end

    local elapsedMillis = lastEventTime and (event.time - lastEventTime) or 0
    lastEventTime = event.time
    accumMillis = accumMillis + elapsedMillis
    if accumMillis > 1000*speed/60 then
        accumMillis = accumMillis - 1000*speed/60
        updatePlayer()
        updateEnemy(1)    
    end
end

local function input( event )
    -- "began" is for touch, "down" is for keys
    if event.phase == "began" or event.phase == "down" then
        dig_dir = clockwiseTurn( dig_dir )
    end
end

Runtime:addEventListener( "enterFrame", update )
Runtime:addEventListener( "key", input )
Runtime:addEventListener( "touch", input )
setup()
draw_tiles()
